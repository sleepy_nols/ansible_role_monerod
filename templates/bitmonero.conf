#
# {{ ansible_managed }}
#

# general
sync-pruned-blocks={{ monerod_sync_pruned_blocks | int }}
prune-blockchain={{ monerod_prune_blockchain | int }}
check-updates={{ monerod_check_updates }}

data-dir={{ monerod_data_dir }}
log-file={{ monerod_log_file }}
{% if monerod_log_level != '' %}log-level={{ monerod_log_level }}{% endif %}
{% if monerod_max_log_file_size != '' and (not monerod_logrotate.enabled | bool) %}max-log-file-size={{ monerod_max_log_file_size }}{% endif %}
{% if monerod_max_log_files != '' and (not monerod_logrotate.enabled | bool) %}max-log-files={{ monerod_max_log_files }}{% endif %}


# anonymity
enable-dns-blocklist={{ monerod_enable_dns_blocklist | int }} # flag
{% if monerod_ban_list != '' %}ban-list={{ monerod_ban_list }}{% endif %} # arg unset

hide-my-port={{ monerod_hide_my_port | int }} # flag
pad-transactions={{ monerod_pad_transactions | int }} # flag
{% if monerod_anonymous_inbound != '' %}anonymous-inbound={{ monerod_anonymous_inbound }}{% endif %} # arg unset
{% if monerod_proxy != '' %}proxy={{ monerod_proxy }}{% endif %} # arg unset
{% if monerod_tx_proxy != '' %}tx-proxy={{ monerod_tx_proxy }}{% endif %} # arg unset
proxy-allow-dns-leaks={{ monerod_proxy_allow_dns_leaks | int }} # flag


# zmq
no-zmq={{ monerod_no_zqm | int }} # flag
zmq-rpc-bind-ip={{ monerod_zmq_rpc_bind_ip }} # arg set
zmq-rpc-bind-port={{ monerod_zmq_rpc_bind_port }} # arg set
{% if monerod_zmq_pub != '' %}zmq-pub={{ monerod_zmq_pub }}{% endif %} # arg unset


# p2p
p2p-bind-ip={{ monerod_p2p_bind_ip }} # arg set
p2p-bind-port={{ monerod_p2p_bind_port }} # arg set
p2p-bind-ipv6-address={{ monerod_p2p_bind_ipv6_address }} # arg set
p2p-bind-port-ipv6={{ monerod_p2p_bind_port_ipv6 }} # arg set
p2p-use-ipv6={{ monerod_p2p_use_ipv6 | int }} # flag
p2p-ignore-ipv4={{ monerod_p2p_ignore_ipv4 | int }} # flag
p2p-external-port={{ monerod_p2p_external_port }} # arg set


# rpc
rpc-use-ipv6={{ monerod_rpc_use_ipv6 | int }} # flag
rpc-ignore-ipv4={{ monerod_rpc_ignore_ipv4 | int}} # flag
{% if monerod_rpc_login != '' %}rpc-login={{ monerod_rpc_login }}{% endif %} # arg unset
confirm-external-bind={{ monerod_confirm_external_bind | int }} # flag

rpc-bind-ip={{ monerod_rpc_bind_ip }} # arg set
rpc-bind-ipv6-address={{ monerod_rpc_bind_ipv6_address }} # arg set
rpc-bind-port={{ monerod_rpc_bind_port }} # arg set

restricted-rpc={{ monerod_restricted_rpc | int }} # flag
rpc-restricted-bind-ip={{ monerod_rpc_restricted_bind_ip }} # arg set
rpc-restricted-bind-ipv6-address={{ monerod_rpc_restricted_bind_ipv6_address }} # arg set
rpc-restricted-bind-port={{ monerod_rpc_restricted_bind_port }} # arg set


# igd
# FIX | 'Cannot have both --no-igd and --igd delayed'
# no-igd={{ monerod_no_idg | int }} # flag
# igd={{ monerod_igd }} # arg set


# limits
max-concurrency={{ monerod_max_concurrency }} # arg set
limit-rate={{ monerod_limit_rate }} # arg set
limit-rate-up={{ monerod_limit_rate_up }} # arg set
limit-rate-down={{ monerod_limit_rate_down }} # arg set
out-peers={{ monerod_out_peers }} # arg set
in-peers={{ monerod_in_peers }} # arg set
tos-flag={{ monerod_tos_flag }} # arg set
max-connections-per-ip={{ monerod_max_connections_per_ip }} # arg set


# modes
public-node={{ monerod_public_node | int }} # flag
offline={{ monerod_offline | int }} # flag
no-sync={{ monerod_no_sync | int }} # flag


# mining
{% if monerod_start_mining != '' %}start-mining={{ monerod_start_mining }}{% endif %} # arg unset
{% if monerod_mining_threads != '' %}mining-threads={{ monerod_mining_threads }}{% endif %} # arg unset
bg-mining-enable={{ monerod_bg_mining_enable | int }} # flag
bg-mining-ignore-battery ={{ monerod_bg_mining_ignore_battery | int }} # flag
{% if monerod_bg_mining_min_idle_interval != '' %}bg-mining-min-idle-interval={{ monerod_bg_mining_min_idle_interval }}{% endif %} # arg unset
{% if monerod_bg_mining_idle_threshold != '' %}bg-mining-idle-threshold={{ monerod_bg_mining_idle_threshold }}{% endif %} # arg unset
{% if monerod_bg_mining_miner_target != '' %}bg-mining-miner-target={{ monerod_bg_mining_miner_target }}{% endif %} # arg unset


# db
db-sync-mode={{ monerod_db_sync_mode }} # arg set
db-salvage={{ monerod_db_salvage | int }} # flag


# testing
testnet={{ monerod_testnet | int }} # flag
stagenet={{ monerod_stagenet | int }} # flag
regtest={{ monerod_regtest | int }} # flag
keep-fakechain={{ monerod_keep_fakechain | int }} # flag
test-drop-download={{ monerod_test_drop_download | int }} # flag
test-drop-download-height={{ monerod_test_drop_download_height }} # arg set
allow-local-ip={{ monerod_allow_local_ip | int }} # flag



# bootstrap
{% if monerod_bootstrap_daemon_address != '' %}bootstrap-daemon-address={{ monerod_bootstrap_daemon_address }}{% endif %} # arg unset
{% if monerod_bootstrap_daemon_login != '' %}bootstrap-daemon-login={{ monerod_bootstrap_daemon_login }}{% endif %} # arg unset
{% if monerod_bootstrap_daemon_proxy != '' %}bootstrap-daemon-proxy={{ monerod_bootstrap_daemon_proxy }}{% endif %} # arg unset


# idk yet
enforce-dns-checkpointing={{ monerod_enforce_dns_checkpointing | int }} # flag
prep-blocks-threads={{ monerod_prep_blocks_threads }} # arg set
fast-block-sync={{ monerod_fast_block_sync | int }} # flag
show-time-stats={{ monerod_show_time_stats | int }} # flag
block-sync-size={{ monerod_block_sync_size }} # arg set
# fluffy-blocks={{ monerod_fluffy_blocks | int }} # flag [outdated,do not use]
no-fluffy-blocks={{ monerod_no_fluffy_blocks | int }} # flag

{% if monerod_add_peer != '' %}add-peer={{ monerod_add_peer }}{% endif %} # arg unset
{% if monerod_add_priority_node != '' %}add-priority-node={{ monerod_add_priority_node }}{% endif %} # arg unset
{% if monerod_add_exclusive_node != '' %}add-exclusive-node={{ monerod_add_exclusive_node }}{% endif %} # arg unset

