# ansible_role_monerod
Ansible role to install and configure a [Monero node](https://www.getmonero.org/) on Debian-like systems.

![ansible-lint](https://gitlab.com/sleepy_nols/ansible_role_monerod/badges/main/pipeline.svg)

This role is part of the [ansible_collection_monero](https://gitlab.com/sleepy_nols/ansible_collection_monero) collection, which you can find at Ansible Galaxy. This role is fully usable outside of the collection.

---
## Role Variables and Defaults






---
## Installing

Install by cloing the repo or using the [ansible_collection_monero](https://gitlab.com/sleepy_nols/ansible_collection_monero) via Ansible Galaxy.

```
git clone git@gitlab.com:sleepy_nols/ansible_role_monerod.git
```

---
## Example Playbooks

### full private node
```yml
- hosts: monero_nodes
  roles:
    - ansible_role_monerod
```

### pruned private node
```yml
- hosts: monero_nodes
  roles:
    - ansible_role_monerod
  vars:
    monerod_prune_blockchain: true
```

### full public node
```yml
- hosts: monero_nodes
  roles:
    - ansible_role_monerod
  vars:
    monerod_public_node: true
    monerod_confirm_external_bind: true
    monerod_rpc_restricted_bind_ip: "0.0.0.0"
    monerod_rpc_restricted_bind_ipv6_address: "::0"
```

### pruned public node
```yml
- hosts: monero_nodes
  roles:
    - ansible_role_monerod
  vars:
    monerod_public_node: true
    monerod_prune_blockchain: true
    monerod_confirm_external_bind: true
    monerod_rpc_restricted_bind_ip: "0.0.0.0"
    monerod_rpc_restricted_bind_ipv6_address: "::0"
```
---
## Contributing

All contributions are welcome. :)

---
## License
GPLv3
